#!/bin/zsh

mkdir txt pdf
rm txt/* pdf

while read palestra; do

  nome=$(echo $palestra | awk -F ',' '{print $3}'| sed 's/\"//g')
  id=$(echo $palestra | awk -F ',' '{print $1}' | sed 's/\"//g')
  data=$(echo $palestra | awk -F ',' '{print $5 " " $6" "}' | sed 's/\"//g')
  datal=$(echo $palestra | awk -F ',' '{print $5}' | sed 's/\"//g' | sed 's/-/\//g')
  datah=$(echo $palestra | awk -F ',' '{print $6 ":" $7}' | awk -F ':' '{print "Horário: "$1"h"$2" às "$4"h"$5}' | sed 's/\"//g')

  nomearq=$(echo ./txt/$data$nome.tex | sed 's/\s/_/g')
  
  while read presenca; do 

    pres=$(echo $presenca | grep $id)

    if [ $pres ]; then

      mat=$(echo $pres | awk -F ',' '{print $1}')
      echo $mat
      cat seccom2016-estudante.csv | grep $mat >> $nomearq
    fi

  done < seccom2016-presenca.csv

  echo $palestra  
  pal=$(sort -u $nomearq)
  echo $pal | sed 's/\"//g' | sed 's/$/\\\\\\/g' > $nomearq
  hedinit=$(echo '\documentclass{article}\n\\\\usepackage[brazil]{babel}\n\\\\usepackage{fontspec}\n\\\\title{'$nome'}\n\\\\author{SECCOM 2016}\n\\\\date{'$datal'}\n\\\\begin{document}\n\\\\maketitle\n'$datah'\\\\\\ \\\\\\ \n Presentes: \\\\\\ \n')
  arq=$(cat $nomearq)
  echo $hedinit'\n'$arq' \n\\end{document}' > $nomearq
  
  xelatex $nomearq
 
done < seccom2016-palestra.csv

mv *.pdf pdf
rm *.{aux,log}
